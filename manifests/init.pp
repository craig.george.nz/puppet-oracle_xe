class oracle_xe (
  $download_url,
  $http_port = 8080,
  $listener_port = 1521,
  $startup = 'y',
  $password,
) {

  $oracle_tmp_dir = "/tmp/oracle-xe"
  $oracle_rpm = "oracle-xe-11.2.0-1.0.x86_64.rpm"
  $oracle_rpm_tmp = "$oracle_tmp_dir/$oracle_rpm"

  file { 'oracle_tmp_dir':
    path        => "$oracle_tmp_dir",
    ensure      => directory,
    before      => Exec[ 'download rpm' ],
  }

  exec { 'download rpm':
    command => "/usr/bin/wget -q $download_url -O $oracle_rpm_tmp",
    creates => "$oracle_rpm_tmp",
  }

  exec { 'create swap file':
    command => "/bin/dd if=/dev/zero of=/var/swap.1 bs=1M count=2048",
    user => root,
    creates => "/var/swap.1",
  }

  exec { 'attach swap file':
    command => "/sbin/mkswap /var/swap.1 && /sbin/swapon /var/swap.1",
    user => root,
    unless => "/sbin/swapon -s | grep /var/swap.1",
    require => Exec["create swap file"],
  }

  # add swap file entry to fstab
  exec { 'add swapfile entry to fstab':
    command => "/bin/echo >>/etc/fstab /var/swap.1 swap swap defaults 0 0",
    user => root,
    unless => "/bin/grep '^/var/swap.1' /etc/fstab 2>/dev/null",
    require => Exec["attach swap file"],
    before  => Package['oracle-xe'],
  }

  service { 'iptables':
    enable    => false,
    ensure    => false,
    hasstatus => true,
  }

  $req_packages = [
    'libaio',
    'bc',
    'flex',
    'glibc',
    'make',
    'binutils',
    'gcc'
  ]

  package { $req_packages:
    ensure => latest,
    before => Package['oracle-xe'],
  }

  package { 'oracle-xe':
    ensure   => present,
    source   => "$oracle_rpm_tmp",
    provider => 'rpm',
  }

  exec { 'oracle-xe-conf':
    creates => '/etc/sysconfig/oracle-xe',
    command => "/usr/bin/printf \"$http_port\\n$listener_port\\n$password\\n$password\\n$startup\\n\" | /etc/init.d/oracle-xe configure",
  }

  service { 'oracle-xe':
    ensure => running,
    enable => true,
    hasrestart => true,
    hasstatus  => true,
  }

  exec { 'set oracle env for vagrant':
    command => "/bin/echo . /u01/app/oracle/product/11.2.0/xe/bin/oracle_env.sh  >> .bashrc" ,
    require => Service['oracle-xe'],
    user    => "vagrant",
  }

  limits::fragment {
    "*/soft/nofile":
      value => "30000";
    "*/hard/nofile":
      value => "30000";
  }

  Exec['download rpm'] -> Package['oracle-xe']
  Package['oracle-xe'] -> Exec['oracle-xe-conf']
  Exec['oracle-xe-conf'] -> Service['oracle-xe']

}
